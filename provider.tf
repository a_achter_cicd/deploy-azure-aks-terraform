terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
    k8s = {
      version = "= 0.8.2"
      source  = "banzaicloud/k8s"
    }
  }
}

provider "k8s" {
  config_context = "prod-cluster"
  load_config_file = false
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name = "albert6-RG"
  location = "germanywestcentral"
}

# Your Terraform code goes here...
