#!/bin/bash

set -euo pipefail


FAILURE=1
SUCCESS=0

ENVIRONMENT="dev"
#SLACKWEBHOOKURL="https://hooks.slack.com/services/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"


CHANNEL_STAGING="gitlabnotifications"
WEBHOOK_STAGING="<WEBHOOK_URL>"

CHANNEL_PRODUCTION="deploy-production"
WEBHOOK_PRODUCTION="<WEBHOOK_URL>"

function print_slack_summary() {

    local slack_msg_header
    local slack_msg_body
    local slack_channel

    # Populate header and define slack channels

    slack_msg_header=":x: CI project ${CI_PROJECT_NAME} Deployment to ${ENVIRONMENT} failed"

    if [[ "${EXIT_STATUS}" == "${SUCCESS}" ]]; then
        slack_msg_header=":white_check_mark: CI project ${CI_PROJECT_NAME} Deployment to ${ENVIRONMENT} - ${CI_JOB_STATUS}"
    fi


    if [[ "${ENVIRONMENT}" == "production" ]]; then
        slack_channel="$CHANNEL_PRODUCTION"
    else
        slack_channel="$CHANNEL_STAGING"
    fi

    # Create slack message body

    slack_msg_body="Job <${CI_JOB_URL}|${CI_JOB_ID}> by ${GITLAB_USER_NAME} \n - Commit: <${CI_PROJECT_URL}/-/commit/$(git rev-parse HEAD)|$(git rev-parse --short HEAD)>\n - Branch: <${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME}|${CI_COMMIT_REF_NAME}>"
    
    cat <<-SLACK
            {
                "channel": "${slack_channel}",
                "blocks": [
                  {
                          "type": "section",
                          "text": {
                                  "type": "mrkdwn",
                                  "text": "${slack_msg_header}"
                          }
                  },
                  {
                          "type": "divider"
                  },
                  {
                          "type": "section",
		                  "text": {
                                  "type": "mrkdwn",
                                  "text": "${slack_msg_body}"
                          }
                  }
                ]
}
SLACK
}

function share_slack_update() {

	local slack_webhook

    slack_webhook="$SLACKWEBHOOKURL"
    
    if [[ "${ENVIRONMENT}" == "production" ]]; then
        slack_webhook="$WEBHOOK_PRODUCTION"
    else
        slack_webhook="$SLACKWEBHOOKURL"
    fi

    curl -X POST                                           \
        --data-urlencode "payload=$(print_slack_summary)"  \
        "${slack_webhook}"
}
