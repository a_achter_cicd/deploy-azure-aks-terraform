resource "azurerm_resource_group" "bertterraformgroup" {
    name     = "netnorm-2-rg"
    location = "Germany West Central"

    tags = {
        environment = "Terraform Demo"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "bertterraformnetwork" {
    name                = "bertVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "Germany West Central"
    resource_group_name = azurerm_resource_group.bertterraformgroup.name

    tags = {
        environment = "Terraform Demo"
    }
}

# Create subnet
resource "azurerm_subnet" "bertterraformsubnet" {
    name                 = "bertSubnet"
    resource_group_name  = azurerm_resource_group.bertterraformgroup.name
    virtual_network_name = azurerm_virtual_network.bertterraformnetwork.name
    address_prefixes       = ["10.0.1.0/24"]
    service_endpoints    = ["Microsoft.Storage"]
}

# Create public IPs
resource "azurerm_public_ip" "bertterraformpublicip" {
    name                         = "bertPublicIP"
    location                     = "Germany West Central"
    resource_group_name          = azurerm_resource_group.bertterraformgroup.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "Terraform Demo"
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "bertterraformnsg" {
    name                = "bertNetworkSecurityGroup"
    location            = "Germany West Central"
    resource_group_name = azurerm_resource_group.bertterraformgroup.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}

# Create network interface
resource "azurerm_network_interface" "bertterraformnic" {
    name                      = "bertNIC"
    location                  = "Germany West Central"
    resource_group_name       = azurerm_resource_group.bertterraformgroup.name

    ip_configuration {
        name                          = "bertNicConfiguration"
        subnet_id                     = azurerm_subnet.bertterraformsubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.bertterraformpublicip.id
    }

    tags = {
        environment = "Terraform Demo"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "bert" {
    network_interface_id      = azurerm_network_interface.bertterraformnic.id
    network_security_group_id = azurerm_network_security_group.bertterraformnsg.id
}


resource "azurerm_linux_virtual_machine" "bertterraformvm" {
  name                            = "vm-bert"
  resource_group_name             = azurerm_resource_group.bertterraformgroup.name
  location                        = "Germany West Central"
  size                            = "Standard_DS1_v2"
  admin_username                  = "azureuser"
  network_interface_ids = [
    azurerm_network_interface.bertterraformnic.id,
  ]

  disable_password_authentication = true
  
  admin_ssh_key {
    username = "azureuser"
    public_key = file(".ssh/id_rsa.pub")
  }

  os_disk {
    name                 = "jumpboxOsDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

}

resource "azurerm_virtual_machine_extension" "helloterraformvm" {
  name                 = "hostname"
  virtual_machine_id   = azurerm_linux_virtual_machine.bertterraformvm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
    {
        "commandToExecute": "sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 && curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && echo 'deb https://apt.kubernetes.io/ kubernetes-xenial main' | sudo tee -a /etc/apt/sources.list.d/kubernetes.list && sudo apt-get update && sudo apt-get install -y kubectl docker.io && curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash"

    }
  SETTINGS

}

#        "commandToExecute": "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -", \
#        "commandToExecute": "echo 'deb https://apt.kubernetes.io/ kubernetes-xenial main' | sudo tee -a /etc/apt/sources.list.d/kubernetes.list", \
#        "commandToExecute": "sudo apt-get update", \
#        "commandToExecute": "sudo apt-get install -y kubectl docker.io", \
#        "commandToExecute": "curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash"

resource "azurerm_managed_disk" "example" {
  name                 = "bert-terraform-test-disk1"
  location             = "Germany West Central"
  resource_group_name  = azurerm_resource_group.bertterraformgroup.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "example" {
  managed_disk_id    = azurerm_managed_disk.example.id
  virtual_machine_id = azurerm_linux_virtual_machine.bertterraformvm.id
  lun                = "10"
  caching            = "ReadWrite"
}
